package com.example.student_card_demo.service;

import com.example.student_card_demo.dto.request.StudentRequestDto;
import com.example.student_card_demo.dto.response.StudentResponseDto;

public interface StudentService {

    StudentResponseDto findById(Long id);

}
