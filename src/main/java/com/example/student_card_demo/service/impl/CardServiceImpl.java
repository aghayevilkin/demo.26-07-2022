package com.example.student_card_demo.service.impl;


import com.example.student_card_demo.dto.request.CardRequestDto;
import com.example.student_card_demo.dto.response.CardResponseDto;
import com.example.student_card_demo.exceptions.CardNotFound;
import com.example.student_card_demo.repository.CardRepository;
import com.example.student_card_demo.service.CardService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CardServiceImpl implements CardService {


private final CardRepository cardRepository;
private final ModelMapper mapper;

    @Override
    public CardResponseDto findById(Long id) {
        return cardRepository.findById(id)
                .map((card) -> mapper.map(card, CardResponseDto.class))
                .orElseThrow(() -> new CardNotFound(id));
    }
}
