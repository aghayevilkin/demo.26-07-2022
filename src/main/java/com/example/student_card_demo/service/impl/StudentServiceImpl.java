package com.example.student_card_demo.service.impl;


import com.example.student_card_demo.dto.request.StudentRequestDto;
import com.example.student_card_demo.dto.response.StudentResponseDto;
import com.example.student_card_demo.exceptions.StudentNotFound;
import com.example.student_card_demo.repository.StudentRepository;
import com.example.student_card_demo.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {


private final StudentRepository studentRepository;
private final ModelMapper mapper;
    @Override
    public StudentResponseDto findById(Long id) {
        return studentRepository.findById(id)
                .map((student) -> mapper.map(student, StudentResponseDto.class))
                .orElseThrow(() -> new StudentNotFound(id));
    }


}
