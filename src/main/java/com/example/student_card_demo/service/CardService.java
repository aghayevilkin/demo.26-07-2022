package com.example.student_card_demo.service;

import com.example.student_card_demo.dto.request.CardRequestDto;
import com.example.student_card_demo.dto.response.CardResponseDto;

public interface CardService {

    CardResponseDto findById(Long id);

}
