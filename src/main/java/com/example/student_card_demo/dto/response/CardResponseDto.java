package com.example.student_card_demo.dto.response;


import com.example.student_card_demo.domain.Student;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CardResponseDto {

    private String name;
    private Student student;

}
