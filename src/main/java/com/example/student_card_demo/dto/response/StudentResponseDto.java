package com.example.student_card_demo.dto.response;


import com.example.student_card_demo.domain.Card;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;


@Getter
@Setter
public class StudentResponseDto {
    private String name;
    private List<Card> cards;

}
