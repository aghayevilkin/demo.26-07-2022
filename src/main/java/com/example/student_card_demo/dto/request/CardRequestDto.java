package com.example.student_card_demo.dto.request;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CardRequestDto {

    private String name;
    private Long studentId;

}
