package com.example.student_card_demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentCardDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(StudentCardDemoApplication.class, args);
    }

}
