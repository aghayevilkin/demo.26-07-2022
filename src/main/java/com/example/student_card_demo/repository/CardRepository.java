package com.example.student_card_demo.repository;

import com.example.student_card_demo.domain.Card;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CardRepository extends JpaRepository<Card,Long> {
}
