package com.example.student_card_demo.exceptions;

public class CardNotFound extends NotFoundException {
    private static final String MESSAGE = "No card with ID %s";

    public CardNotFound(Long id) {
        super(String.format(MESSAGE, id));

    }
}
