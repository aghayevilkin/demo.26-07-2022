package com.example.student_card_demo.exceptions;

public class StudentNotFound extends NotFoundException{

    private static final String MESSAGE = "No studnet with ID %s";

    public StudentNotFound(Long id) {
        super(String.format(MESSAGE, id));

    }
}
