package com.example.student_card_demo.exceptions;


public class NotFoundException extends RuntimeException {

    public NotFoundException(String message){
        super(message);
    }
}
