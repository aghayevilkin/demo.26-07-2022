package com.example.student_card_demo.web.rest;

import com.example.student_card_demo.dto.request.StudentRequestDto;
import com.example.student_card_demo.dto.response.CardResponseDto;
import com.example.student_card_demo.dto.response.StudentResponseDto;
import com.example.student_card_demo.service.CardService;
import com.example.student_card_demo.service.StudentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/card")
@RequiredArgsConstructor
public class CardController {
    private final CardService cardService;

    @GetMapping("/{id}")
    public CardResponseDto getCard(@PathVariable("id") Long id) {
        return cardService.findById(id);
    }

}

