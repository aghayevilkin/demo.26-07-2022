package com.example.student_card_demo.web.rest;


import com.example.student_card_demo.dto.request.CardRequestDto;
import com.example.student_card_demo.dto.response.CardResponseDto;
import com.example.student_card_demo.dto.response.StudentResponseDto;
import com.example.student_card_demo.service.CardService;
import com.example.student_card_demo.service.StudentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/student")
@RequiredArgsConstructor
public class StudentController {

    private final StudentService studentService;

    @GetMapping("/{id}")
    public StudentResponseDto getStudent(@PathVariable("id") Long id) {
        return studentService.findById(id);
    }
}
